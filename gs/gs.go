package gs

import (
	"github.com/spf13/cast"
	"reflect"
)

/**
只支持基本类型 int, string, bool, float32, int64, float64, []byte
支持json转换
*/

type Gs struct {
	v any
	t reflect.Type
}

func (h *Gs) UnmarshalJSON(data []byte) error {
	h.v = data
	return nil
}

func (h *Gs) MarshalJSON() ([]byte, error) {
	return h.ToByte(), nil
}

func NewGs(v any) *Gs {
	return &Gs{v: v}
}

func (h *Gs) ToInt() int {
	if h == nil {
		return 0
	}
	return cast.ToInt(h.v)
}

func (h *Gs) ToStr() string {
	if h == nil {
		return ``
	}
	return cast.ToString(h.v)
}

func (h *Gs) ToBool() bool {
	if h == nil {
		return false
	}
	return cast.ToBool(h.v)
}

func (h *Gs) ToFloat32() float32 {
	if h == nil {
		return 0
	}
	return cast.ToFloat32(h.v)
}

func (h *Gs) ToInt64() int64 {
	if h == nil {
		return 0
	}
	return cast.ToInt64(h.v)
}

func (h *Gs) ToFloat64() float64 {
	if h == nil {
		return 0
	}
	return cast.ToFloat64(h.v)
}

func (h *Gs) ToByte() []byte {
	if h == nil {
		return []byte{}
	}
	return []byte(h.ToStr())
}

func (h *Gs) Type() reflect.Type {
	if h.t == nil {
		h.t = reflect.TypeOf(h.v)
	}
	return h.t
}

func (h *Gs) Val() any {
	if h == nil {
		return nil
	}
	return h.v
}

// IsEmpty 判断是否为空 空字符串 0 0.0
func (h *Gs) IsEmpty() bool {
	if h == nil {
		return true
	}
	if h.v == nil {
		return true
	}
	switch h.Type().Kind() {
	case reflect.String:
		if h.ToStr() == `` {
			return true
		}
		break
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		if h.ToInt64() == 0 {
			return true
		}
	case reflect.Float32, reflect.Float64:
		if h.ToFloat64() == 0.0 {
			return true
		}
	case reflect.Bool:
		if h.ToBool() == false {
			return true
		}
	default:
		return false
	}
	return false
}

func (h *Gs) IsZero() bool {
	if h == nil {
		return true
	}
	if h.v == nil {
		return true
	}
	switch h.Type().Kind() {
	case reflect.String:
		if h.ToInt() == 0 {
			return true
		}
		break
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		if h.ToInt64() == 0 {
			return true
		}
	case reflect.Float32, reflect.Float64:
		if h.ToFloat64() == 0.0 {
			return true
		}
	default:
		return false
	}
	return false
}

func (h *Gs) IsString() bool {
	if h.v == nil {
		return false
	}
	if h.Type().Kind() == reflect.String {
		return true
	} else {
		return false
	}
}
