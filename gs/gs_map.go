package gs

import "github.com/spf13/cast"

type Map struct {
	m map[string]*Gs
}

func NewTransMap[T comparable](m *map[T]any) *Map {
	gsM := &Map{
		m: make(map[string]*Gs),
	}
	for k, v := range *m {
		gsM.m[cast.ToString(k)] = NewGs(v)
	}
	return gsM
}

func (h *Map) G(key any) *Gs {
	if value, ok := h.m[cast.ToString(key)]; ok {
		return value
	}
	return &Gs{}
}
