package gsnsq

import (
	"errors"
	"github.com/nsqio/go-nsq"
)

// NewProducer 创建一个生产者
func NewProducer(config *NsqConfig) (*nsq.Producer, error) {
	if config.PubMsgHost == `` {
		return nil, errors.New(`Publication message address cannot be empty `)
	}
	producerConfig := nsq.NewConfig()
	producer, producerErr := nsq.NewProducer(config.PubMsgHost, producerConfig)
	if producerErr != nil {
		return nil, producerErr
	}
	producer.SetLoggerLevel(nsq.LogLevelError)
	return producer, nil
}
