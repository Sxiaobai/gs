package gsssh

import (
	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
	"io"
	"strings"
)

// RunCommandOnce 建立一个临时session执行一次
func (h *SshConfig) RunCommandOnce(shell string) string {
	var (
		session *ssh.Session
		err     error
	)
	defer func(session *ssh.Session) {
		closeErr := session.Close()
		if closeErr != nil {
			h.Errof(`关闭ssh失败 %s`, closeErr.Error())
		}
	}(session)
	if session, err = h.client.NewSession(); err != nil {
		return err.Error()
	}
	if output, outputErr := session.CombinedOutput(shell); outputErr != nil {
		return outputErr.Error()
	} else {
		return string(output)
	}
}

// UploadFile 上传文件到远程
func (h *SshConfig) UploadFile(remoteFilePath, fileContent string) error {
	var (
		err error
	)
	// 打开SFTP会话
	sftpSession, err := sftp.NewClient(h.client)
	if err != nil {
		h.Errof(`打开sftp失败 %s`, err.Error())
		return err
	}
	// 打开本地文件
	// 如果是文件路径 localFile, err = os.Open(localFilePath) 还要加上close
	var localFile io.Reader
	localFile = strings.NewReader(fileContent)

	// 在远程服务器上创建文件
	remoteFile, err := sftpSession.Create(remoteFilePath)
	if err != nil {
		return err
	}
	defer func(remoteFile *sftp.File) {
		deferError := remoteFile.Close()
		if deferError != nil {
			h.Errof(deferError.Error())
		}
	}(remoteFile)

	// 复制文件内容
	_, err = io.Copy(remoteFile, localFile)
	if err != nil {
		return err
	}
	return nil
}
