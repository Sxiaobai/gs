package gsssh

import (
	"errors"
	"fmt"
	"gitee.com/Sxiaobai/gs/gstool"
	"github.com/gorilla/websocket"
	"golang.org/x/crypto/ssh"
	"runtime/debug"
	"time"
)

const (
	RunTypeTerminal = iota
	RunTypeBridge
)

type SshConfig struct {
	Name             string `json:"name"` //自定义名称
	Host             string `json:"host"`
	Port             string `json:"port"`
	UserName         string `json:"username"`
	Password         string `json:"password"`
	GsSlog           *gstool.GsSlog
	client           *ssh.Client
	isRunning        bool         //是否已运行
	terminal         Terminal     //终端
	bridge           Bridge       //桥接
	runTimeoutTicker *time.Ticker //超时的ticker
	RunType          int
	MaxRunSecond     int //最大执行时间 不超过40秒
}

// NewSshAuthPassword ssh auth password 一个本地端口只允许做一次桥接
func NewSshAuthPassword(host, port, user, password string) *SshConfig {
	return &SshConfig{
		Host:     host,
		Port:     port,
		UserName: user,
		Password: password,
	}
}

// SetMaxRunSecond 设置最大执行超时时间 不能超过40秒
func (h *SshConfig) SetMaxRunSecond(maxRnuSecond int) {
	if maxRnuSecond > 40 {
		maxRnuSecond = 40
	}
	if maxRnuSecond < 0 {
		maxRnuSecond = 40
	}
	h.MaxRunSecond = maxRnuSecond
}

// ConnectAuthPassword ssh auth password
func (h *SshConfig) ConnectAuthPassword() error {
	if h.isRunning {
		return nil
	}
	sshConfig := &ssh.ClientConfig{
		User: h.UserName,
		Auth: []ssh.AuthMethod{
			ssh.Password(h.Password),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(), // In production environments, do not use InsecureIgnoreHostKey
	}
	var sshConnErr error
	h.client, sshConnErr = ssh.Dial("tcp", fmt.Sprintf(`%s:%s`, h.Host, h.Port), sshConfig)
	if sshConnErr != nil {
		h.Errof(`连接失败 %s`, sshConnErr.Error())
		return errors.New(fmt.Sprintf(`ssh conn error：%s`, sshConnErr.Error()))
	}
	//最大执行时间 默认40秒
	if h.MaxRunSecond > 0 {
		h.SetMaxRunSecond(h.MaxRunSecond)
	} else {
		h.SetMaxRunSecond(40)
	}
	h.isRunning = true
	return nil
}

// Close 释放所有资源
func (h *SshConfig) Close() {
	if h.client != nil {
		sshCloseErr := h.client.Close()
		if sshCloseErr != nil {
			//可能会报错 不管
		}
		h.client = nil
	}
	if h.RunType == RunTypeTerminal {
		h.CloseTerminal()
		h.terminal.runStatus = StatusWait
	} else if h.RunType == RunTypeBridge {
		//h.CloseBridge()
	}
	h.isRunning = false
}

func (h *SshConfig) SetSocket(socket *websocket.Conn) {
	h.terminal.Socket = socket
}

func (h *SshConfig) Debugf(msg string, params ...interface{}) {
	if h.GsSlog != nil {
		h.GsSlog.Debugf(gstool.TimeNowUnixToString(`Y-m-d H:i:s`)+` `+msg, params...)
	} else {
		gstool.FmtPrintlnLog(gstool.TimeNowUnixToString(`Y-m-d H:i:s`)+` `+msg, params...)
	}
}

func (h *SshConfig) Errof(msg string, params ...interface{}) {
	if h.GsSlog != nil {
		h.GsSlog.Debugf(gstool.TimeNowUnixToString(`Y-m-d H:i:s`)+` `+msg, params...)
	} else {
		gstool.FmtPrintlnLog(gstool.TimeNowUnixToString(`Y-m-d H:i:s`)+` `+msg, params...)
		debug.PrintStack()
	}
}
