package gsali

import (
	"gitee.com/Sxiaobai/gs/gstool"
	"regexp"
)

type OssQuick struct {
	OssClient *OssConfig
	Business  string
	//临时文件目录
	TempDir string
}

// OssQuickGetUrl OssGetUrl 通过内容获取oss的url
func (h *OssQuick) OssQuickGetUrl(content, business, bucketName string) (string, error) {
	business = h.getBusiness(business)
	h.checkBusiness(business)
	if h.TempDir == `` {
		panic(`临时文件目录不能为空`)
	}
	fileName := gstool.Md5(content)
	localPath := h.TempDir + `/` + fileName + `.txt`
	fileCreateErr := gstool.FileCreate(h.TempDir, fileName+`.txt`, content)
	if fileCreateErr != nil {
		return ``, fileCreateErr
	}
	date := gstool.DateCurrentDate1()
	objectName := business + `/` + date + `/` + fileName + `.txt`
	bucketName = h.getBucketName(bucketName)
	ossUrl, err := h.OssClient.OssClientUploadFile(localPath, objectName, bucketName)
	if err != nil {
		return ``, err
	}
	return ossUrl, nil
}

// OssGetUrlByLocalFile 通过内容获取oss的
func (h *OssQuick) OssGetUrlByLocalFile(localPath, objectName, bucketName string) (string, error) {
	bucketName = h.getBucketName(bucketName)
	ossUrl, err := h.OssClient.OssClientUploadFile(localPath, objectName, bucketName)
	if err != nil {
		return ``, err
	}
	return ossUrl, nil
}

// OssQuickGetContent OssGetContent 通过url获取oss的内容
func (h *OssQuick) OssQuickGetContent(ossUrl, bucketName string) []byte {
	bucketName = h.getBucketName(bucketName)
	content, err := h.OssClient.OssGetFileContent(ossUrl, bucketName)
	if err != nil {
		return nil
	}
	return content
}

func (h *OssQuick) getBusiness(business string) string {
	if business != `` {
		return business
	}
	return h.Business
}

func (h *OssQuick) checkBusiness(business string) {
	if business == `` {
		panic(`业务不能为空`)
	}
	var validateRegex, _ = regexp.Compile("^[a-zA-Z0-9_-]+$")
	matchRet := validateRegex.MatchString(business)
	if !matchRet {
		panic(`业务名称仅支持字母、数字、下划线、中划线`)
	}
}

func (h *OssQuick) getBucketName(bucketName string) string {
	if bucketName != `` {
		return bucketName
	}
	return h.OssClient.BucketName
}
