```shell
#安装方式
go get -u gitee.com/Sxiaobai/gs
```
## 一、mysql

1. 创建连接

```go
//第二个参数true表示开启自动转换字段类型
//Ssh非必需
mysql := gsdb.NewMysql(&gsdb.MysqlConfig{
    Name:     "t",
    Host:     "localhost",
    Port:     3306,
    Username: "root",
    Password: "123456",
    Dbname:   "test", 
    Ssh : &gsssh.SshConfig{
        Host:     `127.0.0.1`,
        Port:     `22`,
        UserName: `root`,
        Password: `123456`,
    },
}, true)
//开启debug 所有执行的sql都将会输出完整日志（优先通过设置的gsLog输出，其次直接输出到标准输出）
//非必须
mysql.OpenDebug()
//设置日志输出 非必须
mysql.SetGsLog(gstool.NewSlogDefault(`/var/www`, `test`))
//设置自定义连接配置 非必须
mysql.SetOpenFunc(func(db *sql.DB) {
    db.SetConnMaxLifetime(time.Hour)
})
err := mysql.CreateConn()

```
2. 增删改查操作
```go
//当开启了autoTrans时条件和设置的值都会根据表结构自动转换为对应类型 mysql本身也支持
one, queryErr := mysql.QuickUpdate(`tbl_user`, map[string]any{
    `id`: [20,21], //会自动转为in
	`username`: []any{`like` , `%1`},
    `username1`: []any{`not in` , []any{1,2,3}},
    `rawsql#1`: []any{`rawsql`, `role_id1 = ?`, []any{`39`}}, //自定义rawsql#1为不重复的字符串，其中的预编译的值不会被自动转换类型
}, map[string]any{
    `role_id1`: `234`,
    `nickname`: ``,
}).Limit(1).Debug().Exec()
```
3. 自动转换类型说明
    > a. 自动转换包括in not in 等各类操作的值，包括查询条件 更新的值  
      b. 自动转换会查询表结构，如果遇到某个字段不存在时会重新更新表结构  
      c. 如果某个字段的类型变更，那么需要重新启动服务，自动转换可能会转换为错误类型

