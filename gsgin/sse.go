package gsgin

import (
	"fmt"
	"gitee.com/Sxiaobai/gs/gstool"
	"github.com/gin-gonic/gin"
	"strings"
	"sync"
	"time"
)

type TSse struct {
	SseList map[string]*Sse
	lock    sync.RWMutex
}

type Event struct {
	EventId string
	Msg     string
}

type Sse struct {
	ClientId           string           //链接ID
	StopC              chan int         //控制gin接口是否退出
	C                  *gin.Context     //c gin
	ChanList           *gstool.ChanList //消息通道
	registerMillSecond int64            //注册时间 毫秒时间戳
	lastEventId        string           //最后发送的eventId
}

func (h *TSse) Register(clientId string, stopC chan int, c *gin.Context) *Sse {
	sse := h.GetSseByClientId(clientId)
	if sse != nil {
		sse.C = c
		sse.StopC = stopC
		sse.ChanList.Active()
		return sse
	}
	chanList := &gstool.ChanList{}
	chanList.Init(10000, func(msg any) error {
		return h.sendMsg(msg.(*Event), sse)
	})
	sse = &Sse{
		ClientId:           clientId,
		StopC:              stopC,
		C:                  c,
		ChanList:           chanList,
		registerMillSecond: time.Now().UnixMilli(),
	}
	h.lock.Lock()
	defer h.lock.Unlock()
	h.SseList[clientId] = sse
	return sse
}

func (h *TSse) sendMsg(e *Event, sse *Sse) (returnErr error) {
	defer func() {
		if err := recover(); err != nil {
			returnErr = gstool.Error(`%s 发送失败 %s`, sse.ClientId, err)
		}
	}()
	sendMsg := e.Msg
	if strings.HasSuffix(sendMsg, "\n\n") {
		sendMsg = strings.TrimSuffix(sendMsg, "\n\n")
		sendMsg = fmt.Sprintf("id:%s\n %s\n\n", e.EventId, sendMsg)
	}
	_, err := sse.C.Writer.Write([]byte(sendMsg))
	if err != nil {
		returnErr = gstool.Error(`%s 写入失败 %s %s`, sse.ClientId, sendMsg, err.Error())
		return
	} else {
		sse.C.Writer.Flush()
		sse.lastEventId = e.EventId
	}
	return
}

func (h *TSse) Send(clientId, msg string) error {
	sse := h.GetSseByClientId(clientId)
	if sse != nil {
		sse.registerMillSecond += 1
		return sse.ChanList.AddMsg(&Event{
			EventId: fmt.Sprintf(`%s_%d`, clientId, sse.registerMillSecond),
			Msg:     msg,
		})
	} else {
		return gstool.Error(`不存在的clientId %s`, clientId)
	}
}

func (h *TSse) SendFormat(clientId, msg string) error {
	return h.Send(clientId, `data: `+msg+"\n\n")
}

func (h *TSse) Pause(clientId string) {
	sse := h.GetSseByClientId(clientId)
	if sse != nil {
		sse.ChanList.Pause()
	}
}

func (h *TSse) GetSseByClientId(clientId string) *Sse {
	h.lock.Lock()
	h.lock.Unlock()
	if sse, ok := h.SseList[clientId]; ok {
		return sse
	}
	return nil
}
