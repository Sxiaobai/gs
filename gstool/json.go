package gstool

import (
	"encoding/json"
	"errors"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"strings"
)

// JsonEncode 加密
func JsonEncode(params interface{}) string {
	str, err := json.Marshal(params)
	if err != nil {
		log.Errorf(`编码json失败 %#v %s`, params, err.Error())
	}
	return cast.ToString(str)
}

// JsonDecode 反解json
func JsonDecode(str string, target any) error {
	err := json.Unmarshal([]byte(str), target)
	if err != nil {
		return errors.New(`error: ` + str)
	}
	return nil
}

// JsonToMap json转为map
func JsonToMap(b []byte) map[string]interface{} {
	m := make(map[string]interface{})
	err := json.Unmarshal(b, &m)
	if err != nil {
		return m
	}
	return m
}

// JsonMarshalTrim json转义字符串中的特殊符号 并移除左侧和右侧的双引号
func JsonMarshalTrim(str string) string {
	//进行转义特殊符号
	b, err := json.Marshal(str)
	if err != nil {
		return str
	} else {
		s := cast.ToString(b)
		s = strings.TrimLeft(s, `"`)
		s = strings.TrimRight(s, `"`)
		return s
	}
}
