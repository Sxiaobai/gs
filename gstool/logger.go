package gstool

import (
	"fmt"
	rotatelogs "github.com/lestrrat/go-file-rotatelogs"
	"github.com/sirupsen/logrus"
	"os"
	"path"
	"time"
)

// Deprecated: 请使用 SlogCreateDefault或者SlogCreate
type GsLogger struct {
	logger   *logrus.Logger
	LogPath  string
	FileName string
}

// CreateLogger 初始化日志 【废弃】
// Deprecated: 请使用 SlogCreateDefault或者SlogCreate
func CreateLogger(logPath, fileName string) *GsLogger {
	logger := logrus.New()
	libLogLogger := &GsLogger{logger: logger, LogPath: logPath, FileName: fileName}
	libLogLogger.InitDefault()
	return libLogLogger
}

// InitDefault 默认值初始化
// Deprecated: 请使用 SlogCreateDefault或者SlogCreate
func (h *GsLogger) InitDefault() {
	//设置日志格式
	h.setTextFormatter(&logrus.TextFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
	})
	h.SetLogLevel(logrus.DebugLevel)
	h.SetRotateLogs(7, time.Hour*24*7, time.Hour*24)
}

// 设置格式
// Deprecated: 请使用 SlogCreateDefault或者SlogCreate
func (h *GsLogger) setTextFormatter(textFormat *logrus.TextFormatter) {
	h.logger.SetFormatter(textFormat)
}

// SetLogLevel 设置日志等级
// Deprecated: 请使用 SlogCreateDefault或者SlogCreate
func (h *GsLogger) SetLogLevel(logLevel logrus.Level) {
	h.logger.SetLevel(logLevel)
}

// 设置输出目录
// Deprecated: 请使用 SlogCreateDefault或者SlogCreate
func (h *GsLogger) setOutputPath(logPath string) {
	src, err := setOutputFile(logPath)
	if err != nil {
		panic(fmt.Sprintf(`初始化日志失败 %s`, err.Error()))
	}
	//设置输出
	h.logger.SetOutput(src)
}

// 转到标准输出
// Deprecated: 请使用 SlogCreateDefault或者SlogCreate
func (h *GsLogger) setOutputStdout() {
	h.logger.SetOutput(os.Stdout)
}

// SetRotateLogs 设置日志切割
// Deprecated: 请使用 SlogCreateDefault或者SlogCreate
func (h *GsLogger) SetRotateLogs(maxCount int, maxAge, maxRotationTime time.Duration) {
	//检测是否存在目录失败
	dirPath := h.LogPath
	dirError := DirCreatePath(dirPath)
	if dirError != nil {
		panic(`创建目录失败 ` + dirError.Error())
	}
	fmt.Println(`设置文件 ` + dirPath + `/` + h.FileName + `%Y%m%d.log`)
	writer, err := rotatelogs.New(
		dirPath+`/`+h.FileName+`%Y%m%d.log`,
		//rotatelogs.WithLinkName(path),				//为最新的日志建立软连接(软链接对应的始终是最新日志)
		rotatelogs.WithRotationCount(maxCount),       //日志最多保留几个
		rotatelogs.WithMaxAge(maxAge),                //日志最多保留多久的
		rotatelogs.WithRotationTime(maxRotationTime), //日志多长时间创建一个
	)
	if err != nil {
		panic(fmt.Sprintf(`初始化日志切割错误 %#v`, err))
	}
	h.logger.SetOutput(writer)
}

// 设置输出到文件
// Deprecated: 请使用 SlogCreateDefault或者SlogCreate
func setOutputFile(logFilePath string) (*os.File, error) {
	now := time.Now()
	_, err := os.Stat(logFilePath)
	if os.IsNotExist(err) {
		if err := os.MkdirAll(logFilePath, 0777); err != nil {
			panic(fmt.Sprintf(`给与日志文件权限失败 %s`, err.Error()))
			return nil, err
		}
	}
	logFileName := now.Format("2006-01-02") + ".log"
	//日志文件
	fileName := path.Join(logFilePath, logFileName)
	if _, err := os.Stat(fileName); err != nil {
		if _, err := os.Create(fileName); err != nil {
			panic(fmt.Sprintf(`创建日志文件失败 %s`, err.Error()))
			return nil, err
		}
	}
	src, err := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		panic(fmt.Sprintf(`打开日志文件失败 %s`, err.Error()))
		return nil, err
	}
	return src, nil
}

// Deprecated: 请使用 SlogCreateDefault或者SlogCreate
func (h *GsLogger) Debugf(msg string, args ...interface{}) {
	if len(args) == 0 {
		h.logger.Debug(msg)
	} else {
		h.logger.Debugf(msg, args...)
	}
}

// Deprecated: 请使用 SlogCreateDefault或者SlogCreate
func (h *GsLogger) Infof(msg string, args ...interface{}) {
	if len(args) == 0 {
		h.logger.Info(msg)
	} else {
		h.logger.Infof(msg, args...)
	}
}

// Deprecated: 请使用 SlogCreateDefault或者SlogCreate
func (h *GsLogger) Warningf(msg string, args ...interface{}) {
	if len(args) == 0 {
		h.logger.Warn(msg)
	} else {
		h.logger.Warningf(msg, args...)
	}
}

// Deprecated: 请使用 SlogCreateDefault或者SlogCreate
func (h *GsLogger) Errorf(msg string, args ...interface{}) {
	if len(args) == 0 {
		h.logger.Error(msg)
	} else {
		h.logger.Errorf(msg, args...)
	}
}
