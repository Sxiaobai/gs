package gstool

import (
	"os"
	"strings"
)

// RunIsBinary 拿到运行类型 go run 执行还是./build/xxx执行 true表示为编译后运行
func RunIsBinary() bool {
	osArgs := os.Args
	if len(osArgs) > 0 {
		return strings.Index(os.Args[0], `/build`) != -1
	}
	return false
}
