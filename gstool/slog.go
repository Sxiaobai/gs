package gstool

import (
	"fmt"
	"gopkg.in/natefinch/lumberjack.v2"
	"io"
	"log"
	"path/filepath"
	"runtime"
)

type GsSlog struct {
	dLogger                *log.Logger //debug
	iLogger                *log.Logger //info
	wLogger                *log.Logger //warn
	eLogger                *log.Logger //error
	aLogger                *log.Logger //默认的日志输出 全部输出到一个文件
	LogPath                string
	BusinessName           string
	BoolFileByLevel        bool // true 按照等级分类日志 false 统一放一个日志文件中
	BoolSimpleCallPosition bool //true 只显示调用位置的文件和行数 false 显示全路径文件和行数
}

// GsSlogLumberConfig 日志备份切割等
type GsSlogLumberConfig struct {
	LocalTime  bool
	MaxSize    int
	MaxAge     int
	MaxBackups int
	Compress   bool
}

// NewSlogDefault create default slog
func NewSlogDefault(logPath, businessName string) *GsSlog {
	gsSlogLumberConfig := GsSlogLumberConfig{
		LocalTime:  true,
		MaxSize:    100,
		MaxAge:     7,
		MaxBackups: 7,
		Compress:   false,
	}
	return SlogCreate(logPath, businessName, true, true, gsSlogLumberConfig)
}

// SlogCreateDefault 初始化默认
// boolFileByLevel true按照日志级别输出到不同的文件中  false全部输出到一个文件中
func SlogCreateDefault(logPath, businessName string) *GsSlog {
	gsSlogLumberConfig := GsSlogLumberConfig{
		LocalTime:  true,
		MaxSize:    100,
		MaxAge:     7,
		MaxBackups: 7,
		Compress:   false,
	}
	return SlogCreate(logPath, businessName, true, true, gsSlogLumberConfig)
}

// SlogCreate 更多创建日志选项
func SlogCreate(logPath, businessName string, boolFileByLevel, boolSimpleCallPosition bool, gsSlogLumberConfig GsSlogLumberConfig) *GsSlog {
	//检测是否存在目录失败
	dirPath := logPath
	dirError := DirCreatePath(dirPath)
	if dirError != nil {
		panic(`创建目录失败 ` + dirError.Error())
	}
	gsSlog := GsSlog{
		LogPath:                logPath,
		BusinessName:           businessName,
		BoolFileByLevel:        boolFileByLevel,
		BoolSimpleCallPosition: boolSimpleCallPosition,
	}
	//只支持debug  info  error三个级别
	if gsSlog.BoolFileByLevel {
		d := getLumberJack(logPath, businessName, `_debug`, gsSlogLumberConfig)
		gsSlog.dLogger = log.New(io.MultiWriter(d), ``, log.Ldate|log.Ltime)

		i := getLumberJack(logPath, businessName, `_info`, gsSlogLumberConfig)
		gsSlog.iLogger = log.New(io.MultiWriter(i), ``, log.Ldate|log.Ltime)

		w := getLumberJack(logPath, businessName, `_warn`, gsSlogLumberConfig)
		gsSlog.wLogger = log.New(io.MultiWriter(w), ``, log.Ldate|log.Ltime)

		e := getLumberJack(logPath, businessName, `_error`, gsSlogLumberConfig)
		gsSlog.eLogger = log.New(io.MultiWriter(e), ``, log.Ldate|log.Ltime)
	} else {
		a := getLumberJack(logPath, businessName, ``, gsSlogLumberConfig)
		gsSlog.aLogger = log.New(io.MultiWriter(a), ``, log.Ldate|log.Ltime)
	}
	return &gsSlog
}

func getLumberJack(logPath, businessName, level string, gsSlogLumberConfig GsSlogLumberConfig) *lumberjack.Logger {
	return &lumberjack.Logger{
		Filename:   logPath + `/` + businessName + level + ".log",
		LocalTime:  gsSlogLumberConfig.LocalTime,  //是否使用本地时间，否则使用UTC时间
		MaxSize:    gsSlogLumberConfig.MaxSize,    //单个日志文件大小(MB)
		MaxAge:     gsSlogLumberConfig.MaxAge,     //日志存活时长(天)
		MaxBackups: gsSlogLumberConfig.MaxBackups, //旧日志文件的数量
		Compress:   gsSlogLumberConfig.Compress,   //是否对旧日志文件进行压缩
	}
}

func (h *GsSlog) formatFile(file string) string {
	if h.BoolSimpleCallPosition {
		return filepath.Base(file)
	} else {
		return file
	}
}

func (h *GsSlog) Debugf(msg string, args ...interface{}) {
	_, file1, line1, ok1 := runtime.Caller(1)
	file1 = h.formatFile(file1)

	_, file2, line2, ok2 := runtime.Caller(2)
	file2 = h.formatFile(file2)
	callPosition := ``
	if ok1 {
		if h.BoolSimpleCallPosition {
			callPosition = fmt.Sprintf("%s:%d", file1, line1)
		} else {
			callPosition = fmt.Sprintf("%s:%d", file1, line1)
		}
	}

	if ok2 {
		if h.BoolSimpleCallPosition {
			callPosition += ` ` + fmt.Sprintf("%s:%d", file2, line2)
		} else {
			callPosition += ` ` + fmt.Sprintf("%s:%d", file2, line2)
		}
	}
	if h.BoolFileByLevel {
		h.dLogger.Println(callPosition + ` ` + fmt.Sprintf(msg, args...))
	} else {
		h.aLogger.Println(` debug ` + callPosition + ` ` + fmt.Sprintf(msg, args...))
	}
}

func (h *GsSlog) Infof(msg string, args ...interface{}) {
	_, file1, line1, ok1 := runtime.Caller(1)
	file1 = h.formatFile(file1)

	_, file2, line2, ok2 := runtime.Caller(2)
	file2 = h.formatFile(file2)
	callPosition := ``
	if ok1 {
		if h.BoolSimpleCallPosition {
			callPosition = fmt.Sprintf("%s:%d", file1, line1)
		} else {
			callPosition = fmt.Sprintf("%s:%d", file1, line1)
		}
	}

	if ok2 {
		if h.BoolSimpleCallPosition {
			callPosition += ` ` + fmt.Sprintf("%s:%d", file2, line2)
		} else {
			callPosition += ` ` + fmt.Sprintf("%s:%d", file2, line2)
		}
	}
	if h.BoolFileByLevel {
		h.iLogger.Println(callPosition + ` ` + fmt.Sprintf(msg, args...))
	} else {
		h.aLogger.Println(` info ` + callPosition + ` ` + fmt.Sprintf(msg, args...))
	}
}

func (h *GsSlog) Warnf(msg string, args ...interface{}) {
	_, file1, line1, ok1 := runtime.Caller(1)
	file1 = h.formatFile(file1)

	_, file2, line2, ok2 := runtime.Caller(2)
	file2 = h.formatFile(file2)
	callPosition := ``
	if ok1 {
		if h.BoolSimpleCallPosition {
			callPosition = fmt.Sprintf("%s:%d", file1, line1)
		} else {
			callPosition = fmt.Sprintf("%s:%d", file1, line1)
		}
	}

	if ok2 {
		if h.BoolSimpleCallPosition {
			callPosition += ` ` + fmt.Sprintf("%s:%d", file2, line2)
		} else {
			callPosition += ` ` + fmt.Sprintf("%s:%d", file2, line2)
		}
	}
	if h.BoolFileByLevel {
		h.wLogger.Println(callPosition + ` ` + fmt.Sprintf(msg, args...))
	} else {
		h.aLogger.Println(` warn ` + callPosition + ` ` + fmt.Sprintf(msg, args...))
	}
}

func (h *GsSlog) Errof(msg string, args ...interface{}) {
	_, file1, line1, ok1 := runtime.Caller(1)
	file1 = h.formatFile(file1)

	_, file2, line2, ok2 := runtime.Caller(2)
	file2 = h.formatFile(file2)
	callPosition := ``
	if ok1 {
		if h.BoolSimpleCallPosition {
			callPosition = fmt.Sprintf("%s:%d", file1, line1)
		} else {
			callPosition = fmt.Sprintf("%s:%d", file1, line1)
		}
	}

	if ok2 {
		if h.BoolSimpleCallPosition {
			callPosition += ` ` + fmt.Sprintf("%s:%d", file2, line2)
		} else {
			callPosition += ` ` + fmt.Sprintf("%s:%d", file2, line2)
		}
	}
	if h.BoolFileByLevel {
		h.eLogger.Println(callPosition + ` ` + fmt.Sprintf(msg, args...))
	} else {
		h.aLogger.Println(` error ` + callPosition + ` ` + fmt.Sprintf(msg, args...))
	}
}
