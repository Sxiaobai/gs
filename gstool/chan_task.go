package gstool

import "errors"

type ChanStruct struct {
	ChanMaxNum int
	Chan       chan interface{}
	CallFunc   func(interface{})
	isStop     bool
}

// ChanCreate 创建一个并发任务
// chanMaxNum 通道最多可以存储多少个消息
// maxDoNum 多少个协程处理消息，并发数
func ChanCreate(chanMaxNum, maxDoNum int, callFunc func(interface{})) ChanStruct {
	chanStruct := ChanStruct{
		ChanMaxNum: chanMaxNum,
		Chan:       make(chan interface{}, chanMaxNum),
		CallFunc:   callFunc,
	}
	for i := 0; i < maxDoNum; i++ {
		go chanStruct.Do()
	}
	return chanStruct
}

// Add 加入消息
func (h *ChanStruct) Add(msg interface{}) error {
	if h.isStop {
		return errors.New(`通道已经关闭`)
	}
	h.Chan <- msg
	return nil
}

// Do 消费
func (h *ChanStruct) Do() {
	for {
		select {
		case msg, ok := <-h.Chan:
			if !ok {
				break
			} else {
				h.CallFunc(msg)
			}
		}
	}
}

// Stop 关闭
func (h *ChanStruct) Stop() {
	h.isStop = true
	close(h.Chan)
}
