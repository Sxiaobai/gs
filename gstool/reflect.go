package gstool

import "reflect"

// ReflectGetType 获取类型
func ReflectGetType(item any) reflect.Kind {
	return reflect.TypeOf(item).Kind()
}
