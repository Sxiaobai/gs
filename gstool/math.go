package gstool

import (
	"math/rand"
	"time"
)

// RandNumber 生成一个随机数 注意是 start <= result < end
func RandNumber(start int, end int) int {
	randList := RandNumberList(start, end, 1)
	return randList[0]
}

// RandNumberList 生成多个随机数
func RandNumberList(startNumber int, endNumber int, count int) []int {

	if endNumber < startNumber || (endNumber-startNumber) < count {
		return nil
	}
	nums := make([]int, 0)
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for len(nums) < count {
		//生成随机数
		num := r.Intn(endNumber-startNumber) + startNumber

		//查重
		exist := false
		for _, v := range nums {
			if v == num {
				exist = true
				break
			}
		}
		if !exist {
			nums = append(nums, num)
		}
	}
	return nums
}
