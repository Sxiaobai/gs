package gsdb

import (
	"context"
	"errors"
	"gitee.com/Sxiaobai/gs/gsdefine"
	"gitee.com/Sxiaobai/gs/gstool"
	"github.com/redis/go-redis/v9"
	"time"
)

// RedisHash 获取hash中的某一个值
func RedisHash(client *GsRedis, key, subKey string, call func() map[string]string, expire time.Duration) string {
	getRet, getErr := client.Client.HGet(context.Background(), key, subKey).Result()
	if errors.Is(getErr, redis.Nil) {
		callRet := call()
		hsetData := make([]interface{}, 0)
		for mapKey, mapValue := range callRet {
			hsetData = append(hsetData, mapKey, mapValue)
		}
		if len(hsetData) == 0 {
			hsetData = append(hsetData, `-1`, `-1`)
		}
		_, setErr := client.Client.HSet(context.Background(), key, hsetData...).Result()
		if setErr != nil {
			gstool.FmtPrintlnLog(`RedisHash HMSet error : %s`, setErr.Error())
			return ``
		}
		return callRet[subKey]
	}
	if getErr != nil {
		gstool.FmtPrintlnLog(`RedisHash HGet error : %s`, getErr.Error())
		return ``
	}
	return getRet
}

// RedisHashE 获取hash中的某一个值
func RedisHashE(client *GsRedis, key, subKey string, call func() map[string]string, expire time.Duration) (string, error) {
	getRet, getErr := client.Client.HGet(context.Background(), key, subKey).Result()
	if errors.Is(getErr, redis.Nil) {
		callRet := call()
		hsetData := make([]interface{}, 0)
		for mapKey, mapValue := range callRet {
			hsetData = append(hsetData, mapKey, mapValue)
		}
		if len(hsetData) == 0 {
			hsetData = append(hsetData, `-1`, `-1`)
		}
		_, setErr := client.Client.HSet(context.Background(), key, hsetData...).Result()
		if setErr != nil {
			return ``, setErr
		}
		return callRet[subKey], nil
	}
	if getErr != nil {
		return ``, getErr
	}
	return getRet, nil
}

// RedisHashAll 获取hash中的所有值
func RedisHashAll(client *GsRedis, key string, call func() map[string]string, expire time.Duration) map[string]string {
	boolExist, boolExistErr := RedisKeyExist(client, key)
	if boolExistErr != nil {
		gstool.FmtPrintlnLog(`判断是否存在出错 %s`, boolExistErr.Error())
		return make(map[string]string)
	}
	if !boolExist {
		callRet := call()
		hsetData := make([]interface{}, 0)
		for mapKey, mapValue := range callRet {
			hsetData = append(hsetData, mapKey, mapValue)
		}
		if len(hsetData) == 0 {
			hsetData = append(hsetData, `-1`, `-1`)
		}
		_, setErr := client.Client.HSet(context.Background(), key, hsetData...).Result()
		if setErr != nil {
			gstool.FmtPrintlnLog(`设置缓存出错了 %s`, setErr.Error())
			return make(map[string]string)
		}
		//设置有效期
		expireErr := RedisSetExpire(client, key, expire)
		if expireErr != nil {
			gstool.FmtPrintlnLog(`设置有效期出错 %s`, expireErr.Error())
			return make(map[string]string)
		}
		return callRet
	}
	getRet, getErr := client.Client.HGetAll(context.Background(), key).Result()
	if getErr != nil {
		gstool.FmtPrintlnLog(`RedisHashAll HGetAll出错 %s`, getErr.Error())
		return make(map[string]string)
	}
	return getRet
}

// RedisHashAllE 获取hash中的所有值
func RedisHashAllE(client *GsRedis, key string, call func() map[string]string, expire time.Duration) (map[string]string, error) {
	boolExist, boolExistErr := RedisKeyExist(client, key)
	if boolExistErr != nil {
		return make(map[string]string), boolExistErr
	}
	if !boolExist {
		callRet := call()
		hsetData := make([]interface{}, 0)
		for mapKey, mapValue := range callRet {
			hsetData = append(hsetData, mapKey, mapValue)
		}
		if len(hsetData) == 0 {
			hsetData = append(hsetData, `-1`, `-1`)
		}
		_, setErr := client.Client.HSet(context.Background(), key, hsetData...).Result()
		if setErr != nil {
			return nil, setErr
		}
		//设置有效期
		expireErr := RedisSetExpire(client, key, expire)
		if expireErr != nil {
			return make(map[string]string), expireErr
		}
		return callRet, nil
	}
	getRet, getErr := client.Client.HGetAll(context.Background(), key).Result()
	if getErr != nil {
		return nil, getErr
	}
	return getRet, nil
}

// RedisGetKey 获取key的值
func RedisGetKey(client *GsRedis, key string) string {
	getRet, getErr := client.Client.Get(context.Background(), key).Result()
	if errors.Is(getErr, redis.Nil) {
		return ``
	}
	if getErr != nil {
		gstool.FmtPrintlnLog(`RedisKey Get error : %s`, getErr.Error())
		return ``
	}
	return getRet
}

// RedisGetKeyE 获取key的值
func RedisGetKeyE(client *GsRedis, key string) (string, error) {
	return client.Client.Get(context.Background(), key).Result()
}

// RedisGetStringCall 获取redis中的数据
func RedisGetStringCall(client *GsRedis, key string, call func() string, expire time.Duration) string {
	ret, retErr := RedisGetKeyE(client, key)
	if errors.Is(retErr, redis.Nil) {
		result := call()
		if result == `` {
			client.Client.Set(context.Background(), key, gsdefine.RedisNotExist, expire)
		} else {
			client.Client.Set(context.Background(), key, call(), expire)
		}
		return result
	}
	if retErr != nil {
		return ``
	}
	if ret == gsdefine.RedisNotExist {
		return ``
	}
	return ret
}

// RedisGetStringCallE 获取redis中的数据
func RedisGetStringCallE(client *GsRedis, key string, call func() string, expire time.Duration) (string, error) {
	ret, retErr := RedisGetKeyE(client, key)
	if errors.Is(retErr, redis.Nil) {
		result := call()
		if result == `` {
			client.Client.Set(context.Background(), key, gsdefine.RedisNotExist, expire)
		} else {
			client.Client.Set(context.Background(), key, call(), expire)
		}
		return result, nil
	}
	if retErr != nil {
		return ``, retErr
	}
	if ret == gsdefine.RedisNotExist {
		return ``, nil
	}
	return ret, nil
}

// RedisSetNx 获取锁
func RedisSetNx(client *GsRedis, key, value string, expire time.Duration) bool {
	setNxRet, setNxErr := client.Client.SetNX(context.Background(), key, value, expire).Result()
	if setNxErr != nil {
		gstool.FmtPrintlnLog(`RedisSetNx SetNX error : %s`, setNxErr.Error())
		return false
	}
	return setNxRet
}

// RedisSetNxE 获取锁
func RedisSetNxE(client *GsRedis, key, value string, expire time.Duration) (bool, error) {
	setNxRet, setNxErr := client.Client.SetNX(context.Background(), key, value, expire).Result()
	if setNxErr != nil {
		return false, setNxErr
	}
	return setNxRet, nil
}

// RedisKeyExist 判断key是否存在
func RedisKeyExist(client *GsRedis, key string) (bool, error) {
	exists, err := client.Client.Exists(context.Background(), key).Result()
	if err != nil {
		return false, err
	}
	if exists == 1 {
		return true, nil
	} else {
		return false, nil
	}
}

// RedisSetExpire 设置缓存有效期
func RedisSetExpire(client *GsRedis, key string, expire time.Duration) error {
	return client.Client.Expire(context.Background(), key, expire).Err()
}
