package gsdb

import (
	"context"
	"fmt"
	"gitee.com/Sxiaobai/gs/gsssh"
	"github.com/redis/go-redis/v9"
	"time"
)

const RedisKeyString = `string`
const RedisKeyHash = `hash`
const RedisKeyList = `list`
const RedisKeySet = `set`
const RedisKeyZSet = `zset`

type RedisConfig struct {
	Name        string `json:"name"`
	Host        string `json:"host"`
	Port        int64  `json:"port"`
	Password    string `json:"password"`
	PoolSize    int    `json:"poolSize"`
	Default     int    `json:"default"`
	Username    string `json:"userName"`
	DialTimeout int64  `json:"dial_timeout"` //连接超时时间 毫秒
	IdleTimeout int64  `json:"idle_timeout"` //空闲链接超过多少秒会被释放 毫秒
	MaxLifetime int64  `json:"max_lifetime"` //连接的最大生存时间 毫秒
}

type GsRedis struct {
	RedisConfig *RedisConfig
	Client      *redis.Client
	Ssh         *gsssh.SshConfig
}

// CreateConn create a connection
func (h *GsRedis) CreateConn() error {
	if h.Ssh != nil && h.Ssh.Host != `` {
		return h.createConnSshPasswordAuth()
	} else {
		return h.createConnDirect()
	}
}

// ssh bridge password auth
func (h *GsRedis) createConnSshPasswordAuth() error {
	targetHostPort := fmt.Sprintf(`%s:%d`, h.RedisConfig.Host, h.RedisConfig.Port)
	localHostPort, runError := h.Ssh.RunBridge(targetHostPort)
	if runError != nil {
		return runError
	}
	options := &redis.Options{
		Network:         "tcp", // 连接方式，默认使用tcp，可省略
		DialTimeout:     time.Millisecond * time.Duration(h.RedisConfig.DialTimeout),
		ConnMaxIdleTime: time.Millisecond * time.Duration(h.RedisConfig.IdleTimeout),
		ConnMaxLifetime: time.Millisecond * time.Duration(h.RedisConfig.MaxLifetime),
		Addr:            localHostPort,
		Password:        h.RedisConfig.Password,
		DB:              h.RedisConfig.Default,
		PoolSize:        h.RedisConfig.PoolSize,
		Username:        h.RedisConfig.Username,
	}
	return h.dbOpen(options)
}

// direct
func (h *GsRedis) createConnDirect() error {
	options := &redis.Options{
		Network:         "tcp", // 连接方式，默认使用tcp，可省略
		DialTimeout:     time.Millisecond * time.Duration(h.RedisConfig.DialTimeout),
		ConnMaxIdleTime: time.Millisecond * time.Duration(h.RedisConfig.IdleTimeout),
		ConnMaxLifetime: time.Millisecond * time.Duration(h.RedisConfig.MaxLifetime),
		Addr:            fmt.Sprintf(`%s:%d`, h.RedisConfig.Host, h.RedisConfig.Port),
		Password:        h.RedisConfig.Password,
		DB:              h.RedisConfig.Default,
		PoolSize:        h.RedisConfig.PoolSize,
		Username:        h.RedisConfig.Username,
	}
	return h.dbOpen(options)
}

// 创建连接
func (h *GsRedis) dbOpen(options *redis.Options) error {
	h.Client = redis.NewClient(options)
	if pingErr := h.Client.Ping(context.Background()).Err(); nil != pingErr {
		return pingErr
	}
	return nil
}
