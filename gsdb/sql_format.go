package gsdb

import (
	"fmt"
	"gitee.com/Sxiaobai/gs/gs"
	"gitee.com/Sxiaobai/gs/gstool"
	"github.com/spf13/cast"
	"reflect"
	"strings"
	"sync"
)

type SqlFormat struct {
	tableMap       map[string]map[string]string                   //table的字段映射列表
	boolTransType  bool                                           //自动转换类型
	dbType         string                                         //mysql sqlite pgsql
	tableFieldFunc func(string string) (map[string]string, error) //获取表字段映射列表方法
	lock           sync.Mutex
}

func NewSqlFormat(boolTransType bool, dbType string, tableFieldFunc func(string string) (map[string]string, error)) *SqlFormat {
	return &SqlFormat{
		boolTransType:  boolTransType,
		tableFieldFunc: tableFieldFunc,
		dbType:         dbType,
		tableMap:       make(map[string]map[string]string),
	}
}

// FormatQuery 仅支持quick 系列
func (h *SqlFormat) FormatQuery(tableName string, where map[string]any, valueList *[]any) (string, error) {
	defer h.lock.Unlock()
	h.lock.Lock()
	sqlWhere := ``
	placeholderList := make([]string, 0)
	whereList := make([]string, 0)
	for field, value := range where {
		valueType := gs.NewGs(value).Type()
		switch valueType.Kind() {
		case reflect.Slice, reflect.Array:
			if customs, ok := value.([]any); ok {
				formatError := h.formatCustom(tableName, field, customs, &whereList, valueList)
				if formatError != nil {
					return ``, formatError
				}
			} else {
				return ``, gstool.Error(`自定义查询格式错误 %#v`, value)
			}
			break
		default:
			whereList = append(whereList, h.getField(field)+` = `+h.getPlaceholder(&placeholderList))
			transValue, transErr := h.transValue(tableName, field, value)
			if transErr != nil {
				return ``, transErr
			}
			*valueList = append(*valueList, transValue)
			break
		}
	}
	sqlWhere = strings.Join(whereList, ` and `)
	if sqlWhere != `` {
		sqlWhere = ` where ` + sqlWhere
	}
	return sqlWhere, nil
}

func (h *SqlFormat) getPlaceholder(placeholderList *[]string) string {
	if h.dbType == DbTypePgsql {
		*placeholderList = append(*placeholderList, ``)
		return fmt.Sprintf(`$%d`, len(*placeholderList))
	} else {
		return `?`
	}
}

func (h *SqlFormat) getField(field string) string {
	return "`" + field + "`"
}

// FormatInsert 仅支持quick create
func (h *SqlFormat) FormatInsert(tableName string, insert map[string]any) (string, string, []any, error) {
	defer h.lock.Unlock()
	h.lock.Lock()
	placeholderList := make([]string, 0)
	insertFieldList := make([]string, 0)
	insertValueList := make([]any, 0)
	quesList := make([]string, 0)
	for field, value := range insert {
		insertFieldList = append(insertFieldList, h.getField(field))
		quesList = append(quesList, h.getPlaceholder(&placeholderList))
		transValue, transErr := h.transValue(tableName, field, value)
		if transErr != nil {
			return ``, ``, nil, transErr
		}
		insertValueList = append(insertValueList, transValue)
	}
	return strings.Join(insertFieldList, `,`), strings.Join(quesList, `,`), insertValueList, nil
}

// FormatUpdate 仅支持quick update
func (h *SqlFormat) FormatUpdate(tableName string, update map[string]any, valueList *[]any) (string, error) {
	defer h.lock.Unlock()
	h.lock.Lock()
	sqlUpdate := ``
	placeholderList := make([]string, 0)
	updateList := make([]string, 0)
	for field, value := range update {
		updateList = append(updateList, h.getField(field)+` = `+h.getPlaceholder(&placeholderList))
		transValue, transErr := h.transValue(tableName, field, value)
		if transErr != nil {
			return ``, transErr
		}
		*valueList = append(*valueList, transValue)
	}
	sqlUpdate = strings.Join(updateList, ` , `)
	return sqlUpdate, nil
}

// 自定义查询类型处理
func (h *SqlFormat) formatCustom(tableName, field string, customList []any, whereList *[]string, valueList *[]any) error {
	opType, paramList, err := h.getOpTypeParams(field, customList)
	if err != nil {
		return err
	}
	placeholderList := make([]string, 0)
	switch opType {
	case `in`, `not in`:
		qusList := make([]string, 0)
		for _, customValue := range paramList {
			qusList = append(qusList, h.getPlaceholder(&placeholderList))
			transValue, transErr := h.transValue(tableName, field, customValue)
			if transErr != nil {
				return transErr
			}
			*valueList = append(*valueList, transValue)
		}
		*whereList = append(*whereList, h.getField(field)+` `+opType+`(`+strings.Join(qusList, `,`)+`)`)
	case `like`, `>`, `<`, `>=`, `<=`, `<>`, `=`:
		transValue, transErr := h.transValue(tableName, field, paramList[0])
		if transErr != nil {
			return transErr
		}
		*valueList = append(*valueList, transValue)
		*whereList = append(*whereList, h.getField(field)+` `+opType+` `+h.getPlaceholder(&placeholderList))
	case `rawsql`: //raw sql 不支持转换
		*whereList = append(*whereList, paramList[0].(string))
		if len(paramList) == 2 && paramList[1] != nil {
			rawValues := cast.ToSlice(paramList[1])
			if len(rawValues) > 0 {
				*valueList = append(*valueList, rawValues...)
			}
		}
	default:
		return nil
	}
	return nil
}

// 两个参数 第一个可能是操作符 也可能只是in的一个参数
func (h *SqlFormat) getOpTypeParams(field string, customList []any) (string, []any, error) {
	length := len(customList)
	if length == 0 {
		return ``, nil, gstool.Error(`错误的参数格式 %s %#v`, field, customList)
	}
	if length == 1 { //一个参数 那么就自动转为=
		return `=`, customList, nil
	}
	param1Type := gs.NewGs(customList[0]).Type().Kind()
	if param1Type != reflect.String { //第一位非字符串只能是默认为in 因为我们的操作符都是字符串形式的 不存在整数的
		return `in`, customList, nil
	}
	param2Type := gs.NewGs(customList[1]).Type().Kind()
	param1 := cast.ToString(customList[0])
	if param2Type == reflect.Array || param2Type == reflect.Slice { //第二位数组 那么只支持in 和not in
		switch param1 {
		case `in`, `not in`:
			return param1, customList[1:], nil
		default:
			return ``, nil, gstool.Error(`错误的格式 %#v`, customList)
		}
	}
	otherOpTypeList := []string{`like`, `>`, `<`, `>=`, `<=`, `<>`, `=`}
	if gstool.ArrayExistValue(&otherOpTypeList, param1) {
		return param1, customList[1:], nil
	} else if param1 == `rawsql` { //自定义sql
		return param1, customList[1:], nil
	} else {
		return `in`, customList, nil
	}

}

// 拿到表字段类型列表
func (h *SqlFormat) getTableFieldTypeList(tableName string) error {
	if _, ok := h.tableMap[tableName]; !ok {
		var err error
		h.tableMap[tableName], err = h.tableFieldFunc(tableName)
		if err != nil {
			return err
		}
		if len(h.tableMap[tableName]) == 0 {
			return gstool.Error(`表字段类型获取失败`)
		}
		for key, val := range h.tableMap[tableName] {
			h.tableMap[tableName][strings.ToLower(key)] = strings.ToLower(val)
		}
	}
	return nil
}

// 转换类型
func (h *SqlFormat) transValue(tableName, fieldName string, value any) (any, error) {
	if !h.boolTransType {
		return value, nil
	}
	if _, ok := h.tableMap[tableName]; !ok {
		getError := h.getTableFieldTypeList(tableName)
		if getError != nil {
			return nil, getError
		}
	}
	if _, ok := h.tableMap[tableName][strings.ToLower(fieldName)]; !ok {
		delete(h.tableMap, tableName)
		getError := h.getTableFieldTypeList(tableName)
		if getError != nil {
			return nil, getError
		}
	}
	if fieldType, ok := h.tableMap[tableName][strings.ToLower(fieldName)]; ok {
		switch fieldType {
		case "int", "tinyint", "smallint", "mediumint", "bigint", "integer":
			return cast.ToInt(value), nil
		case "float", "double", "decimal":
			return cast.ToFloat64(value), nil
		case "date", "datetime", "timestamp":
			return cast.ToString(value), nil
		case "char", "varchar", "tinytext", "text", "mediumtext", "longtext":
			return cast.ToString(value), nil
		default:
			return value, nil
		}
	} else { //字段更新过 返回原值
		return value, nil
	}
}
